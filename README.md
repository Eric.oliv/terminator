

# Solutis Talent Sprint 2020 | Robocode

## Terminator - T1000

+ Realizado em Java por Eric Simão de Oliveira

### Estratégia

<p align="justify"> 
	O T1000 foi programado para disparar um tiro com potência média ao localizar um alvo a distância superior a 150px e um tiro com potência máxima em alvos mais próximos, aumentando a chance de efetuar mais danos no robô inimigo. A rotação do seu canhão em 360° permite uma varredura completa do perímetro em busca de um alvo, ao encontrar, o T1000 se desloca em direção ao inimigo efetuando disparos, e sendo atingido, o mesmo se move para trás recuando do ataque e contra-atacando de imediato.
</p>

### Pontos fortes 

+ Potencializa o disparo contra o adversário de acordo a proximidade
+ Rápido recuo e disparo, quando atingido
+ Varredura do perímetro em busca de inimigos e travamento da mira no adversário

### Pontos fracos 

+ Não prevê um disparo a fim de desviar
+ Quando pressionado em uma aresta apresenta dificuldades para sair

### Experiência

O Talent Sprint 2020 Solutis está sendo um processo diferente de todos que já fiz, toda a equipe tem mostrado um grande empenho e afinco na elaboração das etapas e no cuidado com a transmissão das informações. A proposta de programar um robô para competição, uniu diversão e aprendizado (Já recomendei o Robocode para batalhar com amigos), achei muito proveitoso e desafiador, uma ótima experiência para explorar meus conhecimentos e habilidades. 
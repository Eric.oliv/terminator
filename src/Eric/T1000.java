package terminator;

import robocode.Robot;
import robocode.*;
import robocode.util.*;
import java.awt.Color;
import robocode.HitRobotEvent;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;
import static robocode.util.Utils.normalRelativeAngleDegrees;


/**
 * T1000 - a robot by Eric Sim�o de Oliveira
 */
public class T1000 extends Robot
{
	boolean peek; 
	double moveAmount; 
	int count = 0; 
	double gunTurnAmt; 

    public void run() {
		moveAmount = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
		peek = false;
		setAdjustGunForRobotTurn(true); 
		gunTurnAmt = 10; 
		turnLeft(getHeading() % 90);
		ahead(moveAmount);
		peek = true;
		turnGunRight(90);
		turnRight(90);
		
        while (true) {
			turnGunRight(gunTurnAmt);
			count++;
			if (count > 2) {
				gunTurnAmt = -10;
			}
			if (count > 5) {
				gunTurnAmt = 10;
			}
			turnGunRight(360);
        }
    }
 
    public void onScannedRobot(ScannedRobotEvent e) {
		count = 0;

		if (e.getDistance() > 150) {
			gunTurnAmt = normalRelativeAngleDegrees(e.getBearing() + (getHeading() - getRadarHeading()));
			turnGunRight(gunTurnAmt); 
			fire(2);
			return;
		}

		gunTurnAmt = normalRelativeAngleDegrees(e.getBearing() + (getHeading() - getRadarHeading()));
		turnGunRight(gunTurnAmt);
		fire(3);
		
		if (e.getDistance() < 100) {
			if (e.getBearing() > -90 && e.getBearing() <= 90) {
				back(200);
			} else {
				ahead(200);
			}
		}
		scan();
    }

		
	public void onHitRobot(HitRobotEvent e) {
		if (e.getBearing() > -90 && e.getBearing() < 90) {
			back(200);
		} 
		else {
			ahead(100);
		}
	}
		
	public void onHitWall(HitWallEvent e) {
			if (e.getBearing() > -90 && e.getBearing() < 90) {
			back(100);
		} 
		else {
			ahead(200);
		}
	}
			
	public void onHitByBullet(HitByBulletEvent e) {
		
			if (e.getBearing() > -90 && e.getBearing() < 90) {
			back(200);
		} 
		else {
			ahead(200);
		}
		turnRight(normalRelativeAngleDegrees(90 - (getHeading() - e.getHeading())));
	}
		
	public void onWin(WinEvent e) {
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
	}
}